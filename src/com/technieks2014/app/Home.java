package com.technieks2014.app;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.StrictMode;
import android.os.Vibrator;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.telephony.TelephonyManager;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.SherlockFragmentActivity;

public class Home extends SherlockFragmentActivity implements SensorEventListener {

	private SensorManager sensorManager;
	private long lastUpdate;
	ActionBar mActionBar;
    ViewPager mPager;
    public String PhoneNumber = "";
    Tab tab;
    int i = 0;
    
	@Override
    public void onCreate(Bundle savedInstanceState)
    {
		

        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_main);
        
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        
        mActionBar = getSupportActionBar();
        mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        mActionBar.hide();
        
        mPager = (ViewPager) findViewById(R.id.pager);
        FragmentManager fm = getSupportFragmentManager();
        
        ViewPager.SimpleOnPageChangeListener ViewPagerListener = new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                // Find the ViewPager Position
                mActionBar.setSelectedNavigationItem(position);
            }
        };
        
        mPager.setOnPageChangeListener(ViewPagerListener);
        mPager.setOffscreenPageLimit(2);
        ViewPagerAdapter viewpageradapter = new ViewPagerAdapter(fm);
        mPager.setAdapter(viewpageradapter);
        ActionBar.TabListener tabListener = new ActionBar.TabListener() {
        	 
            @Override
            public void onTabSelected(Tab tab, FragmentTransaction ft) {
                // Pass the position on tab click to ViewPager
                mPager.setCurrentItem(tab.getPosition());
            }
 
            @Override
            public void onTabUnselected(Tab tab, FragmentTransaction ft) {
                // TODO Auto-generated method stub
            }
 
            @Override
            public void onTabReselected(Tab tab, FragmentTransaction ft) {
                // TODO Auto-generated method stub
            }
        };
        
        
        tab = mActionBar.newTab().setText("Day 1").setTabListener(tabListener);
        mActionBar.addTab(tab);
        
        tab = mActionBar.newTab().setText("Day 2").setTabListener(tabListener);
        mActionBar.addTab(tab);
        
        tab = mActionBar.newTab().setText("Day 3").setTabListener(tabListener);
        mActionBar.addTab(tab);
        
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        lastUpdate = System.currentTimeMillis();
        
    }

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		// TODO Auto-generated method stub
		if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
		      getAccelerometer(event);
		    }
	}

	private void getAccelerometer(SensorEvent event) {
		// TODO Auto-generated method stub
		float[] values = event.values;
	    // Movement
	    float x = values[0];
	    float y = values[1];
	    float z = values[2];
	    
	    float accelationSquareRoot = (x * x + y * y + z )
	            / (SensorManager.GRAVITY_EARTH * SensorManager.GRAVITY_EARTH);
	    long actualTime = System.currentTimeMillis();
	    if (accelationSquareRoot >= 2) {
	       if (actualTime - lastUpdate < 350) {
	         return;
	       }
	       lastUpdate = actualTime;
	       i = i + 1;
	       
	       if( i == 3 ){
	    	   Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
	    	   v.vibrate(500);
			   postData();
			   Intent me = new Intent("com.technieks2014.app.HIDDENONE");
			   Bundle bh = new Bundle();
			   bh.putString("msg", "You have activated the First hidden feature of the app. Approach the reception with a screenshot of this page to claim your prize\n(Note: Your number (" + PhoneNumber + ") has been recorded).\n \nGood luck finding the other feature(s)!\n");
			   me.putExtras(bh);
	           startActivity(me);
	    	   //String strI = Integer.toString(i);
		       //Toast.makeText(getApplicationContext(), strI+" so start!" , Toast.LENGTH_SHORT).show();
		   }
	    }
	}
	
	@Override
	protected void onResume() {
	  super.onResume();
	  // register this class as a listener for the orientation and
	  // accelerometer sensors
	  sensorManager.registerListener(this,
	    sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
	    SensorManager.SENSOR_DELAY_NORMAL);
	}
	
	@Override
	protected void onPause() {
	  // unregister listener
	  super.onPause();
	  overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
	  sensorManager.unregisterListener(this);
	}
	
	public void postData() {
	    // Create a new HttpClient and Post Header
	    HttpClient httpclient = new DefaultHttpClient();
	    HttpPost httppost = new HttpPost("http://technieks.com/android/saveNumber1.php");

	    try {
	        // Add your data
	        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		       TelephonyManager tMgr =(TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		       String mPhoneNumber = tMgr.getLine1Number();
		       PhoneNumber = mPhoneNumber;
	        nameValuePairs.add(new BasicNameValuePair("number", mPhoneNumber));
	        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

	        // Execute HTTP Post Request
	        HttpResponse response = httpclient.execute(httppost);

	    } catch (ClientProtocolException e) {
	        // TODO Auto-generated catch block
	    } catch (IOException e) {
	        // TODO Auto-generated catch block
	    }
	}
	
}