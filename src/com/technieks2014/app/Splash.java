package com.technieks2014.app;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

public class Splash extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
	    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
	        WindowManager.LayoutParams.FLAG_FULLSCREEN);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
		
		MediaPlayer Intro = MediaPlayer.create(Splash.this, R.raw.intro);
        Intro.start();
        Thread timer = new Thread() {
           public void run() {
              try {
                 sleep(3500);
              } catch(InterruptedException e) {
                 e.printStackTrace();
              } finally {
                 Intent o = new Intent("com.technieks2014.app.MAIN");
                 startActivity(o);
                 overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
              }
           }
        };
        timer.start();
	}
	
	@Override
    protected void onPause() {
       super.onPause();
       finish();
       overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
	
}
