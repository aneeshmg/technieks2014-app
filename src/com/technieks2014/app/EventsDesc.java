package com.technieks2014.app;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class EventsDesc extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
	    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
	        WindowManager.LayoutParams.FLAG_FULLSCREEN);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.events_desc);
		
		
		Bundle b = getIntent().getExtras();
        final Activity activity = this;
        String Imgurl = b.getString("imgUrl");
        final String RegUrl = b.getString("regUrl");
        String Desc = b.getString("desc");
        String Name = b.getString("name");
        
        TextView Header = (TextView) findViewById(R.id.eventHeadText);
        TextView DescTV = (TextView) findViewById(R.id.eventDescText);
        Button Reg = (Button) findViewById(R.id.eventRegBut);
        ImageView Poster = (ImageView) findViewById(R.id.EImageView);
        
        Typeface or = Typeface.createFromAsset(getAssets(), "fonts/ostrich-regular.ttf");
        Typeface cb = Typeface.createFromAsset(getAssets(), "fonts/corbert.ttf");
        
        Header.setText(Name);
        DescTV.setText(Desc);
        
        Header.setTypeface(or);
        DescTV.setTypeface(cb);
        Reg.setTypeface(or);
        
        Bitmap bmp = null;
		try {
			bmp = BitmapFactory.decodeStream(new URL(Imgurl).openConnection().getInputStream());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        Poster.setImageBitmap(bmp);
        
        Reg.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
            	//Toast.makeText(EventsDesc.this, RegUrl, Toast.LENGTH_SHORT).show();
            	
            	Intent ms = new Intent("com.technieks2014.app.TWEETY");
				Bundle bs = new Bundle();
				bs.putString("url", RegUrl);
				ms.putExtras(bs);
                startActivity(ms);
                overridePendingTransition(R.anim.right_in, R.anim.left_out);
            	
            }
        });
	}
	
	@Override
    protected void onPause() {
       super.onPause();
       finish();
       overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
}
