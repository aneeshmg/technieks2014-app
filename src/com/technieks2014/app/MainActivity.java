package com.technieks2014.app;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Vibrator;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements android.view.View.OnClickListener {

	Button enter,about,sponcors,gallery,tweety,fb;
	TextView Updates;
	String update = "";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
	    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
	        WindowManager.LayoutParams.FLAG_FULLSCREEN);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		Toast.makeText(MainActivity.this, "Find hidden features to win prizes!", Toast.LENGTH_LONG).show();
		Toast.makeText(MainActivity.this, "Activate internet to enable all features", Toast.LENGTH_SHORT).show();
		
		
		TextView header = (TextView) findViewById(R.id.mainHeader);
		Updates = (TextView) findViewById(R.id.mainUpdate);
		enter = (Button) findViewById(R.id.mainEnter);
		about = (Button) findViewById(R.id.mainAbout);
		sponcors = (Button) findViewById(R.id.mainSponcors);
		gallery = (Button) findViewById(R.id.mainGallery);
		tweety = (Button) findViewById(R.id.mainTweety);
		fb = (Button) findViewById(R.id.mainFb);
		
		Typeface or = Typeface.createFromAsset(getAssets(), "fonts/ostrich-regular.ttf");
		Typeface cb = Typeface.createFromAsset(getAssets(), "fonts/corbert.ttf");
		
		header.setTypeface(or);
		Updates.setTypeface(cb);
		enter.setTypeface(or);
		about.setTypeface(or);
		sponcors.setTypeface(or);
		
		enter.setOnClickListener(this);
		about.setOnClickListener(this);
		sponcors.setOnClickListener(this);
		tweety.setOnClickListener(this);
		gallery.setOnClickListener(this);
		fb.setOnClickListener(this);
		
		getUpdate();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		Dialog d = new Dialog(this);
        d.setTitle("Done by");
        TextView tv = new TextView(this);
        tv.setText("Web and Android Team \n techNIEks 2014");
        tv.setTextSize(15);
        tv.setPadding(10, 5, 10, 6);
        d.setContentView(tv);
        d.show();
		return false;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
			case R.id.mainEnter:
				Intent me = new Intent("com.technieks2014.app.HOME");
                startActivity(me);
				break;
				
			case R.id.mainAbout:
				Intent ma = new Intent("com.technieks2014.app.ABOUT");
                startActivity(ma);
				break;
				
			case R.id.mainSponcors:
				Intent ms = new Intent("com.technieks2014.app.TWEETY");
				Bundle bs = new Bundle();
				bs.putString("url", "http://technieks.com/android/Sponsors.html");
				ms.putExtras(bs);
                startActivity(ms);
                overridePendingTransition(R.anim.right_in, R.anim.left_out);
				break;
				
			case R.id.mainTweety:
				Intent mt = new Intent("com.technieks2014.app.TWEETY");
				Bundle bt = new Bundle();
				bt.putString("url", "http://technieks.com/android/displayTweets.html");
				mt.putExtras(bt);
                startActivity(mt);
                overridePendingTransition(R.anim.right_in, R.anim.left_out);
				break;
				
			case R.id.mainGallery:
				Intent mg = new Intent("com.technieks2014.app.TWEETY");
				Bundle bg = new Bundle();
				bg.putString("url", "http://technieks.com/android/gallery.html");
				mg.putExtras(bg);
                startActivity(mg);
                overridePendingTransition(R.anim.right_in, R.anim.left_out);
				break;
				
			case R.id.mainFb:
				Intent mf = new Intent("com.technieks2014.app.TWEETY");
				Bundle bf = new Bundle();
				bf.putString("url", "http://m.facebook.com/techNIEks");
				mf.putExtras(bf);
                startActivity(mf);
                overridePendingTransition(R.anim.right_in, R.anim.left_out);
				break;
		}
		
		enter.setOnLongClickListener(new View.OnLongClickListener() {

	        @Override
	        public boolean onLongClick(View v) {

	            //Toast.makeText(MainActivity.this, "Unlocking hidden feature", Toast.LENGTH_LONG).show();
	        	
	        	Vibrator Vma1 = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
		    	   Vma1.vibrate(500);
				   postData(2);
				   Intent me = new Intent("com.technieks2014.app.HIDDENONE");
				   Bundle bh = new Bundle();
				   TelephonyManager tMgrMA =(TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
				   bh.putString("msg", "You have activated the second hidden feature of the app. Approach the reception with a screenshot of this page to claim your prize\n(Note: Your number (" + tMgrMA.getLine1Number() + ") has been recorded).\n \nGood luck finding the other feature(s)!\n");
				   me.putExtras(bh);
		           startActivity(me);

	            return true;
	        }
	    });
		
		gallery.setOnLongClickListener(new View.OnLongClickListener() {

	        @Override
	        public boolean onLongClick(View v) {

	            //Toast.makeText(MainActivity.this, "Unlocking hidden feature", Toast.LENGTH_LONG).show();
	        	
	        	Vibrator Vma2 = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
		    	   Vma2.vibrate(500);
				   postData(3);
				   Intent me = new Intent("com.technieks2014.app.HIDDENONE");
				   Bundle bh = new Bundle();
				   TelephonyManager tMgrMA =(TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
				   bh.putString("msg", "You have activated the third hidden feature of the app. Approach the reception with a screenshot of this page to claim your prize\n(Note: Your number (" + tMgrMA.getLine1Number() + ") has been recorded).\n \nGood luck finding the other feature(s)!\n");
				   me.putExtras(bh);
		           startActivity(me);

	            return true;
	        }
	    });
		
	}
	
	
	public void getUpdate(){
		Updater updateTask = new Updater();
		updateTask.execute("http://technieks.com/android/update.php");
	}
	
	private class Updater extends AsyncTask<String, Void, String>{

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			
			String response = "";
	        HttpPost httppost = new HttpPost(params[0]);
	        HttpClient client = new DefaultHttpClient();
            try {
            	HttpResponse execute = client.execute(httppost);
                InputStream content = execute.getEntity().getContent();
                BufferedReader buffer = new BufferedReader(new InputStreamReader(content));
                String s = "";
                while ((s = buffer.readLine()) != null) {
                  response += s;
                }
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
	        
			return response;
		}
		
		@Override
	    protected void onPostExecute(String result) {
			
			update = result;
			Updates.setText("Updates:- " + result);
	    }
		
	}
	
	public void postData(int i) {
	    // Create a new HttpClient and Post Header
	    HttpClient httpclient = new DefaultHttpClient();
	    HttpPost httppost = new HttpPost("http://technieks.com/android/saveNumber" + i + ".php");

	    try {
	        // Add your data
	        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		       TelephonyManager tMgr =(TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		       String mPhoneNumber = tMgr.getLine1Number();
	        nameValuePairs.add(new BasicNameValuePair("number", mPhoneNumber));
	        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

	        // Execute HTTP Post Request
	        HttpResponse response = httpclient.execute(httppost);

	    } catch (ClientProtocolException e) {
	        // TODO Auto-generated catch block
	    } catch (IOException e) {
	        // TODO Auto-generated catch block
	    }
	}
	
}