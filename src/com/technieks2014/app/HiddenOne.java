package com.technieks2014.app;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

public class HiddenOne extends Activity {

	@Override
    public void onCreate(Bundle savedInstanceState)
    {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
	    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
	        WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hidden_one);
        
        
        Bundle b = getIntent().getExtras();
        final Activity activity = this;
        String message = b.getString("msg");
        
        Dialog d = new Dialog(this);
        d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        //d.getWindow().clearFlags(LayoutParams.FLAG_DIM_BEHIND);
        d.setTitle("Congratulations");
        TextView tv = new TextView(this);
        tv.setText(message);
        tv.setTextColor(Color.parseColor("#000000"));
        tv.setTextSize(15);
        tv.setPadding(10, 5, 10, 6);
        d.setContentView(tv);
        d.show();
        
        
    }
	
	@Override
    protected void onPause() {
       super.onPause();
       finish();
       overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
	
}
