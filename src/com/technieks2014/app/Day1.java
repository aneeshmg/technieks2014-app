package com.technieks2014.app;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragment;
 
public class Day1 extends SherlockFragment {
 
	
	private ProgressDialog pDialog;
	 
    // URL to get contacts JSON
    public static String url = "http://technieks.com/android/data/eventsD1.json";
    
    ListView list;
    ArrayList<HashMap<String, String>> newItemlist = new ArrayList<HashMap<String, String>>();
    private static final String TAG_EVENT = "events";
    View view;
    JSONArray newitem;
    Typeface cb;
    Typeface or;
    
    LinearLayout layout;
    
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        // Get the view from fragmenttab1.xml
        view = inflater.inflate(R.layout.day1, container, false);
        
        layout = (LinearLayout) view.findViewById(R.id.buttonview1);
        
        TextView header = (TextView) view.findViewById(R.id.day1Header);
        
        or = Typeface.createFromAsset(getActivity().getAssets(), "fonts/ostrich-regular.ttf");
        cb = Typeface.createFromAsset(getActivity().getAssets(), "fonts/corbert.ttf");
        
        header.setTypeface(or);
        
        pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Loading ...");
        newItemlist = new ArrayList<HashMap<String, String>>();
    
        new JSONParse().execute();
        
        if(isOnline()){
        	//Toast.makeText(getActivity(), "net present", Toast.LENGTH_SHORT).show();
        }
        else {
        	Toast.makeText(getActivity(), "No network Access!", Toast.LENGTH_SHORT).show();
        }
        
        return view;
    }
 
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        setUserVisibleHint(true);
    }
 
    
    private class JSONParse extends AsyncTask<String, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
           pDialog.show();
        }
        @Override
        protected Void doInBackground(String... args) {
            try {
                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpGet httpPost = new HttpGet(url);
                HttpResponse httpResponse = httpClient.execute(httpPost);
                HttpEntity httpEntity = httpResponse.getEntity();
                String jsonstring = EntityUtils.toString(httpEntity);
                
                JSONObject json = new JSONObject(jsonstring);
                
                newitem = json.getJSONArray(TAG_EVENT);  

            } catch (Exception ex){
            }
            
            return null;

        }
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            pDialog.dismiss();
            
            
            try{
            	Log.i("number",Integer.toString(newitem.length()));
            	Button tv[] = new Button[newitem.length()];
                for(int i = 0; i < newitem.length(); i++){
                   JSONObject c = newitem.getJSONObject(i);
                   final String url = c.getString("url");
                   final String imgUrl = c.getString("img-url");
                   final String desc = c.getString("description");
                   final String name = c.getString("name");
                   
                   tv[i] = new Button(getActivity());
                   tv[i].setText(newitem.getJSONObject(i).getString("name"));
                   tv[i].setTypeface(cb);
                   tv[i].setTextSize(25);
                   
                   tv[i].setOnClickListener(new View.OnClickListener() {

                       @Override
                       public void onClick(View v) {
                       	
                   			//Toast.makeText(getActivity(), url, Toast.LENGTH_SHORT).show();
                    	   	Intent d1 = new Intent("com.technieks2014.app.EVENTSDESC");
           					Bundle d1b = new Bundle();
           					d1b.putString("regUrl", url);
           					d1b.putString("imgUrl", imgUrl);
           					d1b.putString("desc", desc);
           					d1b.putString("name", name);
           					d1.putExtras(d1b);
           					startActivity(d1);
                       }
                   });
                   
                   layout.addView(tv[i]);
                                      
               }
            } catch(Exception ex)
            {
            }
         }
    }
    
    private boolean isOnline() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getActivity().getSystemService( Context.CONNECTIVITY_SERVICE );
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }
}