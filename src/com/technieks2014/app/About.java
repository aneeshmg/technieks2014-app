package com.technieks2014.app;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

public class About extends Activity {

	@Override
    public void onCreate(Bundle savedInstanceState)
    {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
	    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
	        WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about);
        
        TextView Desc = (TextView) findViewById(R.id.technieksDesc);
        TextView Header = (TextView) findViewById(R.id.aboutHeader);
        
        Typeface or = Typeface.createFromAsset(getAssets(), "fonts/ostrich-regular.ttf");
        Typeface cb = Typeface.createFromAsset(getAssets(), "fonts/corbert.ttf");
        
        Desc.setTypeface(cb);
        Header.setTypeface(or);
    }
	
	@Override
    protected void onPause() {
       super.onPause();
       finish();
       overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
}
