package com.technieks2014.app;

import android.webkit.WebView;
import android.webkit.WebViewClient;

public class WebClient extends WebViewClient {

   @Override
   public boolean shouldOverrideUrlLoading(WebView v,String url) {
      v.loadUrl(url);
      return true;
   }
   
   /*@Override
   public void onPageFinished(WebView view, String url) {
       super.onPageFinished(view, url);
       view.clearCache(true);
   }*/

}